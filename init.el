(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#292A30" "#FC6A5D" "#67B7A4" "#D0BF68" "#5DD8FF" "#D0A8FF" "#8abeb7" "#FFFFFF"])
 '(custom-safe-themes
   '("d7ee1fdb09a671a968b2a751746e5b3f5f26ac1fd475d95d094ee1e4ce446d58" "8146edab0de2007a99a2361041015331af706e7907de9d6a330a3493a541e5a6" "a6e620c9decbea9cac46ea47541b31b3e20804a4646ca6da4cce105ee03e8d0e" "1a52e224f2e09af1084db19333eb817c23bceab5e742bf93caacbfea5de6b4f6" "4b0e826f58b39e2ce2829fab8ca999bcdc076dec35187bf4e9a4b938cb5771dc" "fe2539ccf78f28c519541e37dc77115c6c7c2efcec18b970b16e4a4d2cd9891d" "a0be7a38e2de974d1598cf247f607d5c1841dbcef1ccd97cded8bea95a7c7639" "9687f29504a36c0b6b46cf654117f2f2ab3e73b909476ccb14cdde2bf990fa3e" "4f1d2476c290eaa5d9ab9d13b60f2c0f1c8fa7703596fa91b235db7f99a9441b" "cf922a7a5c514fad79c483048257c5d8f242b21987af0db813d3f0b138dfaf53" "f6665ce2f7f56c5ed5d91ed5e7f6acb66ce44d0ef4acfaa3a42c7cfe9e9a9013" "1d44ec8ec6ec6e6be32f2f73edf398620bb721afeed50f75df6b12ccff0fbb15" "c5ded9320a346146bbc2ead692f0c63be512747963257f18cc8518c5254b7bf5" "e2c926ced58e48afc87f4415af9b7f7b58e62ec792659fcb626e8cba674d2065" "d6844d1e698d76ef048a53cefe713dbbe3af43a1362de81cdd3aefa3711eae0d" "5f19cb23200e0ac301d42b880641128833067d341d22344806cdad48e6ec62f6" "47db50ff66e35d3a440485357fb6acb767c100e135ccdf459060407f8baea7b2" "da53441eb1a2a6c50217ee685a850c259e9974a8fa60e899d393040b4b8cc922" "a7b20039f50e839626f8d6aa96df62afebb56a5bbd1192f557cb2efb5fcfb662" "6c386d159853b0ee6695b45e64f598ed45bd67c47f671f69100817d7db64724d" "88f59acbeacefb4998f45126d4d8ae8b2184f2a48753db362a349fd55321c7e1" "850bb46cc41d8a28669f78b98db04a46053eca663db71a001b40288a9b36796c" "40b03c9cb9d50e4d7539dfc1968e2e5943279bd643fd7e92d974efb53a18a302" "c2aeb1bd4aa80f1e4f95746bda040aafb78b1808de07d340007ba898efa484f5" "d268b67e0935b9ebc427cad88ded41e875abfcc27abd409726a92e55459e0d01" "52498cb107fb252e92ac35b8a5eba466fede7f03bebcf50412190c452ed7329e" "4133d2d6553fe5af2ce3f24b7267af475b5e839069ba0e5c80416aa28913e89a" "1278c5f263cdb064b5c86ab7aa0a76552082cf0189acf6df17269219ba496053" "b0e446b48d03c5053af28908168262c3e5335dcad3317215d9fdeb8bac5bacf9" "6f4421bf31387397f6710b6f6381c448d1a71944d9e9da4e0057b3fe5d6f2fad" "4a5aa2ccb3fa837f322276c060ea8a3d10181fecbd1b74cb97df8e191b214313" "e19ac4ef0f028f503b1ccafa7c337021834ce0d1a2bca03fcebc1ef635776bea" "0466adb5554ea3055d0353d363832446cd8be7b799c39839f387abb631ea0995" "4b6b6b0a44a40f3586f0f641c25340718c7c626cbf163a78b5a399fbe0226659" "b5803dfb0e4b6b71f309606587dd88651efe0972a5be16ece6a958b197caeed8" "d47f868fd34613bd1fc11721fe055f26fd163426a299d45ce69bef1f109e1e71" "266ecb1511fa3513ed7992e6cd461756a895dcc5fef2d378f165fed1c894a78c" "8d7b028e7b7843ae00498f68fad28f3c6258eda0650fe7e17bfb017d51d0e2a2" "6b1abd26f3e38be1823bd151a96117b288062c6cde5253823539c6926c3bb178" "5784d048e5a985627520beb8a101561b502a191b52fa401139f4dd20acb07607" "1f1b545575c81b967879a5dddc878783e6ebcca764e4916a270f9474215289e5" "b7e460a67bcb6cac0a6aadfdc99bdf8bbfca1393da535d4e8945df0648fa95fb" "f302eb9c73ead648aecdc1236952b1ceb02a3e7fcd064073fb391c840ef84bca" "97db542a8a1731ef44b60bc97406c1eb7ed4528b0d7296997cbb53969df852d6" "cbdf8c2e1b2b5c15b34ddb5063f1b21514c7169ff20e081d39cf57ffee89bc1e" "22a514f7051c7eac7f07112a217772f704531b136f00e2ccfaa2e2a456558d39" "1d78d6d05d98ad5b95205670fe6022d15dabf8d131fe087752cc55df03d88595" "b186688fbec5e00ee8683b9f2588523abdf2db40562839b2c5458fcfb322c8a4" "0b3aee906629ac7c3bd994914bf252cf92f7a8b0baa6d94cb4dfacbd4068391d" "3d54650e34fa27561eb81fc3ceed504970cc553cfd37f46e8a80ec32254a3ec3" "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13" "f7fed1aadf1967523c120c4c82ea48442a51ac65074ba544a5aefc5af490893b" "1704976a1797342a1b4ea7a75bdbb3be1569f4619134341bd5a4c1cfb16abad4" "9f9fc38446c384a4e909b7220d15bf0c152849ef42f5b1b97356448612c77953" "5aef652e40fa5f111e78997285f6e4c892112da0c2f919eb663baaa330a8521f" "1f50a7274cd56f28713e1694600ec7b8f2fd1c7d2ef38c5e7378a26931605409" "6e14157d0c8857e81035e6c7131dc17e4115b3911c82a1fd32e528aec8e89eab" "26e07f80888647204145085c4fed78e0e6652901b62a25de2b8372d71de9c0a1" "846b3dc12d774794861d81d7d2dcdb9645f82423565bfb4dad01204fa322dbd5" "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae" default))
 '(exwm-floating-border-color "#161616")
 '(fci-rule-color "#5c5e5e")
 '(highlight-tail-colors ((("#2f383b") . 0) (("#32383d") . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#0d0d0d" "#5DD8FF"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#0d0d0d" "#67B7A4"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#0d0d0d" "#6C7986"))
 '(objed-cursor-color "#FC6A5D")
 '(package-selected-packages
   '(visual-fill-column smooth-scrolling nlinum csharp-mode dap-mode github-theme lorem-ipsum hl-todo docker-compose-mode dockerfile-mode lsp-python-ms all-the-icons neotree centaur-tabs vterm-toggle ox-epub vterm magit evil-collection evil xclip which-key use-package rainbow-delimiters ivy-rich ivy-posframe ivy-hydra doom-themes counsel-projectile counsel-codesearch avy))
 '(pdf-view-midnight-colors (cons "#FFFFFF" "#292A30"))
 '(rustic-ansi-faces
   ["#292A30" "#FC6A5D" "#67B7A4" "#D0BF68" "#5DD8FF" "#D0A8FF" "#8abeb7" "#FFFFFF"])
 '(vc-annotate-background "#292A30")
 '(vc-annotate-color-map
   (list
    (cons 20 "#67B7A4")
    (cons 40 "#8ab990")
    (cons 60 "#adbc7c")
    (cons 80 "#D0BF68")
    (cons 100 "#dfaf5a")
    (cons 120 "#ee9f4c")
    (cons 140 "#FD8F3F")
    (cons 160 "#ee977f")
    (cons 180 "#df9fbf")
    (cons 200 "#D0A8FF")
    (cons 220 "#de93c9")
    (cons 240 "#ed7e93")
    (cons 260 "#FC6A5D")
    (cons 280 "#d86d67")
    (cons 300 "#b47171")
    (cons 320 "#90757b")
    (cons 340 "#5c5e5e")
    (cons 360 "#5c5e5e")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(setq inhibit-startup-message t)

(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)
(menu-bar-mode -1)
(defalias 'yes-or-no-p 'y-or-n-p)

;; font
(set-face-attribute 'default nil :font "JetBrains Mono" :height 140)

;; set font for emoji
(set-fontset-font
 t
 '(#x1f300 . #x1fad0)
 (cond
  ((member "Noto Color Emoji" (font-family-list)) "Noto Color Emoji")
  ((member "Noto Emoji" (font-family-list)) "Noto Emoji")
  ((member "Segoe UI Emoji" (font-family-list)) "Segoe UI Emoji")
  ((member "Symbola" (font-family-list)) "Symbola")
  ((member "Apple Color Emoji" (font-family-list)) "Apple Color Emoji")))

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8-unix)

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("gnu" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-insall 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; clipboard
(use-package xclip)
(xclip-mode 1)

;; numbers
(use-package nlinum
  :config
  (setq nlinum-format "%5d ")
  (global-nlinum-mode))
(dolist (mode '(org-mode-hook
		term-mode-hook
		neotree-mode-hook
		vterm-mode-hook
		shell-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (nlinum-mode 0))))

;; IVY
(use-package ivy
  :diminish ""
  :bind (:map ivy-minibuffer-map
              ("C-w" . ivy-yank-word) ;; make work like isearch
              ("C-r" . ivy-previous-line))
  :config
  (ivy-mode 1)
  (setq ivy-initial-inputs-alist nil) ;; no regexp by default
  (setq ivy-re-builders-alist         ;; allow input not in order
        '((t . ivy--regex-ignore-order)))
  :custom
  (ivy-count-format "(%d/%d) ")
  (ivy-mode t)
  (ivy-use-selectable-prompt t)
  (ivy-use-virtual-buffers t))

;; visual things
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package doom-themes)
(load-theme 'doom-tomorrow-night) ;; dark
;(load-theme 'doom-homage-white) ;; light

;; scrolling
(setq redisplay-dont-pause t
  mouse-wheel-scroll-amount '(1 ((shift) . 1))
  mouse-wheel-follow-mouse 't
  scroll-margin 1
  scroll-step 1
  scroll-conservatively 10000
  scroll-preserve-screen-position 1)

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idlr-delay 0.3))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
	 ("C-x b" . counsel-ibuffer)
	 ("C-x C-f" . counsel-find-file)
	 :map minibuffer-local-map
	 ("C-r" . 'counsel-minibuffer-history)))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode nil)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  (defun my/kill-this-buffer ()
    (interactive)
    (kill-buffer (current-buffer)))
  (evil-ex-define-cmd "q" 'my/kill-this-buffer)
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))


(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package magit
  :commands (magit-status magit-get-current-branch))

(defun efs/org-mode-setup ()
  ;(org-indent-mode)
  (variable-pitch-mode 0)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (setq evil-auto-indent nil))

(defun efs/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•")))))))

(use-package org
  :hook
  (org-mode . efs/org-mode-setup)
  :config
  (efs/org-font-setup)
  (setq org-ellipsis " ▾")
  (setq org-hide-emphasis-markers nil)
  (setq org-agenda-files '("~/Sync/Org/"))
  (setq org-todo-keywords '((type "TODO(t)" "NEXT(n)" "|" "DONE(d)"))))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 80
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

(require 'ox-latex)
(setq org-latex-listings t)

(use-package vterm)
(put 'dired-find-alternate-file 'disabled nil)

(use-package vterm-toggle)
(global-set-key (kbd "C-x v") 'vterm-toggle)

(use-package magit)
(use-package all-the-icons)
(use-package neotree
  :config
  (setq neo-theme 'arrow)
  :bind
  (:map evil-normal-state-map
	     ("F" . neotree-toggle)))

(use-package python-mode
  :custom
  (python-shell-interpreter "python3"))


;; Configure indent for C
(defun my-c-mode-hook ()
  (setq-default tab-width 4)
  (setq-default indent-tabs-mode t)
  (setq-default c-default-style "linux")
  (c-set-offset 'substatement-open 0)
  (setq-default c-basic-offset 4)
  (setq c-basic-offset 4))
(add-hook 'c-mode-hook 'my-c-mode-hook)
(global-set-key (kbd "TAB") 'tab-to-tab-stop)
